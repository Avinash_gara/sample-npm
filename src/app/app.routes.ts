import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
// import { PageNotFoundComponent } from './not-found.component';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewComponent } from './view/view.component';
import { LoginComponent } from './login/login.component';


export const AppRoutes: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full' },
{ path:'app-root', component:AppComponent,},
{ path:'dashboard', component:DashboardComponent},
{ path:'view', component:ViewComponent },
{ path:'login', component: LoginComponent}
];

export const AppRouteRoot = RouterModule.forRoot(AppRoutes, { useHash: true });