import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Router,Routes } from '@angular/router';
import { AppRouteRoot } from './app.routes';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewComponent } from './view/view.component';
// import { Response, Http ,ResponseContentType} from '@angular/http';
import { SharedModule } from './shared/shared.module';
import { SharedServicesModule } from './shared/shared-services/shared-services.module';
import {HttpModule} from '@angular/http';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ChartsModule } from 'ng2-charts';
import {DatepickerModule} from 'ngx-date-picker';
import { LoginComponent } from './login/login.component';
import { NgDatepickerModule } from 'ng2-datepicker';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ViewComponent,
    LoginComponent
  
  ],
  imports: [
    NgDatepickerModule,
    DatepickerModule,
    ChartsModule,
    Ng2SmartTableModule,
    BrowserModule,
    RouterModule, 
    AppRouteRoot,
    SharedModule.forRoot(),
    SharedServicesModule.forRoot(),
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
